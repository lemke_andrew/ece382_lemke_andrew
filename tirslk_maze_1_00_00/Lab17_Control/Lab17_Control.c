// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// September 12, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/TimerA0.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/FlashProgram.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"
//#include "../inc/SysTick.h"
//#include "../inc/Classifier.c"


/**************Initial values used for all programs******************/
//Part 1: 5000, 1800
#define PWMNOMINAL 5000 // worked for 3 7000 // for final 2 3700 // was 4000 //used 7000 and 3000 for part 1, 5000 and 1000 for part 2
#define SWING 800 // worked for 3 3000 // for final2 1000 // was 1100  for 4000 // was 3300
#define PWMIN (PWMNOMINAL- SWING)
#define PWMAX (PWMNOMINAL+ SWING)

volatile uint32_t ControllerFlag; // set every 10ms on controller execution

int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec

// proportional controller gain 
// experimentally determine value that creates a stable system
// may want a different gain for each program
int32_t Kp1= 70; // works for 3 8; // was 70, lowing for final part 3;

/**************Functions used by all programs***********************/
void Pause3(void){
    int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // restart Jacki
  UR = UL = PWMNOMINAL;    // reset parameters
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}

void Cop_Car_Mode(void){
    // also known as party mode
  while(1){
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(1); // red
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
}

/**************Program17_1******************************************/
#define DESIRED_SPEED 80 // was 100
#define TACHBUFF 10                      // number of elements in tachometer array

int32_t ActualSpeedL, ActualSpeedR;   	 // Actual speed
int32_t ErrorL, ErrorR;     			 // X* - X'
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)

int i = 0;

void LCDClear1(void){
    Nokia5110_Init();
    Nokia5110_Clear();
    Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Error(RPM)  L     R     ");
}
void LCDOut1(void){
    Nokia5110_SetCursor(1, 1);         // one leading space, second row
    Nokia5110_OutSDec1(DESIRED_SPEED);
    Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
    Nokia5110_OutSDec1(DESIRED_SPEED);
    Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
    Nokia5110_OutUDec(ActualSpeedL);
    Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
    Nokia5110_OutUDec(ActualSpeedR);
    Nokia5110_SetCursor(1, 5);       // zero leading spaces, sixth row
    Nokia5110_OutSDec1(ErrorL);
    Nokia5110_SetCursor(7, 5);       // six leading spaces, sixth row
    Nokia5110_OutSDec1(ErrorR);
}

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;
  for(i=0; i<length; i=i+1){
    sum = sum + array[i];
  }
  return (sum/length);
}

//**************************************************
// Proportional controller to drive straight with no
// sensor input
void Controller1(void){

    if(Mode){
		// pull tachometer information
        Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
        i++;
        if (i == TACHBUFF) {
            i = 0;
        }

		// use average of ten tachometer values to determine 
		// actual speed (similar to Lab16)
        ActualSpeedL = 2000000 / avg(LeftTach, TACHBUFF);
        ActualSpeedR = 2000000 / avg(RightTach, TACHBUFF);
		
		
		// use proportional control to update duty cycle
		// LeftDuty = LeftDuty + Kp*LeftError
        ErrorR = DESIRED_SPEED - ActualSpeedR; // x* - x'
        ErrorL = DESIRED_SPEED - ActualSpeedL; // x* - x'

        /*
         *  so the problem araises when we calculate the Error for the ActualSpeedR, it is returned in RPM,
         *  but the UR is in terms of PWM, so
         */


		UR = UR + Kp1 * ErrorR;
		UL = UL + Kp1 * ErrorL;

		// check min/max duty values
		if (UR < 2) {
		    UR = 2;
		}
		if (UR > 14998) {
		    UR = 14998;
		}
		if (UL < 2) {
		    UL = 2;
		}
		if (UL > 14998) {
		    UL = 14998;
		}

		// update motor values
        Motor_Forward(UL, UR);
		ControllerFlag = 1;

    }
}

// go straight with no sensor input
void Program17_1(void){
  DisableInterrupts();
// initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Tachometer_Init();
  Motor_Init();
  
  // user TimerA1 to run the controller at 100 Hz
  // replace this line with a call to TimerA1_Init()
  TimerA1_Init(&Controller1, 5000); // 500000 / [num in func call] = [hertz you want to operate at]

  Motor_Stop();
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  LCDClear1();
  ControllerFlag = 0;
  Pause3();

	while(1){
		if(Bump_Read()){
			Mode = 0;
			Motor_Stop();
			Pause3();
		}
		if(ControllerFlag){
			LCDOut1();
			ControllerFlag = 0;
		}
  }
}

/**************Program17_2******************************************/
// distances in mm
#define TOOCLOSE 110
#define DESIRED_DIST 172
#define TOOFAR 230
#define Kp2 8

volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm
volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosError;
int32_t Error;
int32_t ActualL, ActualR;

void LCDClear2(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("E= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
}

void LCDOut2(void){
  Nokia5110_SetCursor(3,2); Nokia5110_OutSDec(Left);
  Nokia5110_SetCursor(3,3); Nokia5110_OutSDec(Center);
  Nokia5110_SetCursor(3,4); Nokia5110_OutSDec(Right);
  Nokia5110_SetCursor(3,5); Nokia5110_OutSDec(Error);
  // left
  if(Time%5 == 0){
      UART0_OutUDec5(Left);UART0_OutString(" mm,");
      UART0_OutUDec5(Center);UART0_OutString(" mm,");
      UART0_OutUDec5(Right);UART0_OutString(" mm,");
      UART0_OutUDec5(UR);UART0_OutString(" %,");
      UART0_OutUDec5(UL);UART0_OutString(" %,");
      if(Error < 0){
          PosError = Error*(-1);
          UART0_OutString("-");UART0_OutUDec5(PosError);UART0_OutString("\n");
      }
      else{
          UART0_OutUDec5(Error);UART0_OutString("\n");
      }

  }
}

void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_12_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
//void SysTick_Handler(void){
//    if(Mode){
//		// Determine set point
//		if (Left > 172 && Right > 172) {
//		    SetPoint = (Left + Right) / 2;
//		}
//		else {
//		    SetPoint = 172;
//		}
//
//		// set error based off set point
//		if (Left > Right) {
//		    Error = Left - SetPoint;
//		}
//		else if (Right > Left) {
//		    Error = SetPoint - Right;
//		}
//
//        // use average of ten tachometer values to determine
//        // actual speed (similar to Lab16)
////        ActualSpeedL = 2000000 / avg(LeftTach, TACHBUFF);
////        ActualSpeedR = 2000000 / avg(RightTach, TACHBUFF);
//
//
//		// update duty cycle based on proportional control
//		UR = UR + Kp2 * Error / 1;
//		UL = UL - Kp2 * Error / 1;
//
//		// check to ensure not too big of a swing
//        if (UR < PWMIN) {
//            UR = PWMIN;
//        }
//        if (UR > PWMAX) {
//            UR = PWMAX;
//        }
//        if (UL < PWMIN) {
//            UL = PWMIN;
//        }
//        if (UL > PWMAX) {
//            UL = PWMAX;
//        }
//
//		// update motor values
//        Motor_Forward(UL, UR);
//        ControllerFlag = 1;
//    }
//}

// proportional control, wall distance
void Program17_2(void){
    uint32_t raw17,raw12,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Motor_Init();
	
	// user TimerA1 to sample the IR sensors at 2000 Hz
    TimerA1_Init(&IRsampling, 250); // 500000 / [num in func call] = [hertz you want to operate at]
	
    Motor_Stop();
    LCDClear2();
    Mode = 0;
    UR = UL = PWMNOMINAL;
    ADCflag = ControllerFlag = 0;   // semaphores

    ADC0_InitSWTriggerCh17_12_16();   // initialize channels 17,12,16
    ADC_In17_12_16(&raw17,&raw12,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw12,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

	// user SysTick to run the controller at 100 Hz with a priority of 2
	// replace this line with a call to SysTick_Init()
    //SysTick_Init(480000, 2);
    //COMMENTED OUT THE NEXT LINE FOR FINAL PART 4 TO WORK
    //SysTick_Init();

	
    Pause3();

    EnableInterrupts();
    while(1){
        if(Bump_Read()){ // collision
            Mode = 0;
            Motor_Stop();
            Pause3();
        }
        if(ControllerFlag){ // 100 Hz, not real time
            LCDOut2();
            ControllerFlag = 0;
        }
    }
}

/**************Program17_3******************************************/

uint8_t LineData;       // direct measure from line sensor
int32_t Position;      // position in 0.1mm relative to center of line
#define CHANGEVAL 4    // the increment of change if the robot is too far left or right // worked with 4
#define CHANGESWING 1000 //worked with 3000

void LCDClear3(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("Line Follow");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("D =  "); Nokia5110_OutUDec(0);
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("P = "); Nokia5110_OutSDec(0);
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("UR=  "); Nokia5110_OutUDec(0);
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("UL=  "); Nokia5110_OutUDec(0);
}
void LCDOut3(void){
  Nokia5110_SetCursor(5,2); Nokia5110_OutUDec(LineData);
  Nokia5110_SetCursor(4,3); Nokia5110_OutSDec(Position);
  Nokia5110_SetCursor(5,4); Nokia5110_OutUDec(UR);
  Nokia5110_SetCursor(5,5); Nokia5110_OutUDec(UL);
}

/*
* Proportional controller to drive robot 
* using line following
*/
int32_t Change=0;
volatile uint32_t systickCount = 0;
void Controller3(void){
	
	// read values from line sensor, similar to
	// SysTick_Handler() in Lab10_Debugmain.c
    // Reftectance start on one systick ISR, End() on the next
        if (systickCount == 8) {
            Reflectance_Start();
        }
        else if (systickCount == 9) {
             // bits go left to right, 1 for black 0 for while so 0xc0 means the robot is to the right of the line with the left 2 sensors on black
            LineData = Reflectance_End();
              //Bump = Bump_Read(); // not used in lab 17.3
        }
        if (systickCount == 9) {
            systickCount = 0;
        }
        else {
            systickCount++;
        }

	// Use Reflectance_Position() to find position
        Position =  Reflectance_Position(LineData);

	
    if(Mode){
        // Looks like Change is the acceleration check,
        // like how fast can your robot swerve over once you start stepping out
		// if robot is too far right of line (Position > 0)
		// increase change
        if (Position > 0) {
            Change += CHANGEVAL * Position;
        }

		// if robot is too far left of line (Position < 0)
		// decrease change
        else {
            Change += CHANGEVAL * Position;
        }

		// limit change to within swing
		if (Change > CHANGESWING) {
		    Change = CHANGESWING;
		} else if (Change < -1 * CHANGESWING) {
		    Change = -1 * CHANGESWING;
		}

		// update duty cycle based on porportional control
        UR = UR + Change;
		UL = UL - Change;

		// so I think checking UR is sort of the speed check,
		// like how muc different in pwm can the two motors turn
        // check to ensure not too big of a swing
        if (UR < PWMIN) {
            UR = PWMIN;
        }
        if (UR > PWMAX) {
            UR = PWMAX;
        }
        if (UL < PWMIN) {
            UL = PWMIN;
        }
        if (UL > PWMAX) {
            UL = 5000;
        }
        //UL = 5000;
        //UR = 5000;

        Motor_Forward(UL, UR);
		ControllerFlag = 1;
    }
}

// proportional control, line following
void Program17_3(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    
	// user TimerA1 to run the controller at 1000 Hz, this is every 0.0001s
    TimerA1_Init(&Controller3, 500); // 500000 / [num in func call] = [hertz you want to operate at]
	
    Motor_Stop();
    LCDClear3();
    Mode = 0;
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();
    Pause3();
    while(1){
      if(Bump_Read()){ // collision
        Mode = 0;
        Motor_Stop();
        Pause3();
      }
      if(ControllerFlag){ // 100 Hz , not real time
        LCDOut3();
        ControllerFlag = 0;
      }
    }
}




// Maze Project Part 1
/**************Program17_4******************************************/

uint8_t LineData;       // direct measure from line sensor
int32_t Position;      // position in 0.1mm relative to center of line
#define M1CHANGEVAL 4    // the increment of change if the robot is too far left or right // worked with 4
#define M1CHANGESWING 1000 //worked with 3000
int time = 0;
int newTime = 0;
int bumpCount = 0;
int backDist = 50;
int minBackPWM = 3000;
int maxBackPWM = 4000;
int hasDone180 = 0;
int hasDoneBack = 0;


//unessisary LCD functions removed, already defined in 3

// backs straight up distance in mm
void Backwards() {
    if (Mode == 2) {
        Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
                i++;
                if (i == TACHBUFF) {
                    i = 0;
                }

                // use average of ten tachometer values to determine
                // actual speed (similar to Lab16)
                ActualSpeedL = 2000000 / avg(LeftTach, TACHBUFF);
                ActualSpeedR = 2000000 / avg(RightTach, TACHBUFF);

                // use proportional control to update duty cycle
                // LeftDuty = LeftDuty + Kp*LeftError
                ErrorR = DESIRED_SPEED - ActualSpeedR; // x* - x'
                ErrorL = DESIRED_SPEED - ActualSpeedL; // x* - x'

                UR = UR + Kp1 * ErrorR;
                UL = UL + Kp1 * ErrorL;

                // check min/max duty values
                if (UR < PWMIN) {
                    UR = PWMIN;
                }
                if (UR > PWMAX) {
                    UR = PWMAX;
                }
                if (UL < PWMIN) {
                    UL = PWMIN;
                }
                if (UL > PWMAX) {
                    UL = PWMAX;
                }

                // update motor values
                Motor_Backward(UL, UR);
                ControllerFlag = 1;

                // check to see when done. 360 steps per roation, 70*pi mm per rotation (219.91mm)
                // compiler error if you use an #define in an if statement
                if ( ( backDist > (LeftSteps * 220 / 360)) && ( backDist > (RightSteps * 220 / 360)) ) {
                    Motor_Stop();
                    hasDoneBack = 1;
                    Mode = 0;
            }
    }
}

//int32_t Change=0; // already defined
//volatile uint32_t systickCount = 0;   // already defined in 3
void Controller4(void){

    // read values from line sensor, similar to
    // SysTick_Handler() in Lab10_Debugmain.c
    // Reftectance start on one systick ISR, End() on the next
        if (systickCount == 8) {
            Reflectance_Start();
        }
        else if (systickCount == 9) {
             // bits go left to right, 1 for black 0 for while so 0xc0 means the robot is to the right of the line with the left 2 sensors on black
            LineData = Reflectance_End() ^ 0xFF;
              //Bump = Bump_Read(); // not used in lab 17.3
        }
        if (systickCount == 9) {
            systickCount = 0;
        }
        else {
            systickCount++;
        }

    // Use Reflectance_Position() to find position
        Position =  Reflectance_Position(LineData);


    if(Mode == 1){
        // Looks like Change is the acceleration check,
        // like how fast can your robot swerve over once you start stepping out
        // if robot is too far right of line (Position > 0)
        // increase change
        if (Position > 0) {
            Change += M1CHANGEVAL * Position;
        }

        // if robot is too far left of line (Position < 0)
        // decrease change
        else {
            Change += M1CHANGEVAL * Position;
        }

        // limit change to within swing
        if (Change > M1CHANGESWING) {
            Change = M1CHANGESWING;
        } else if (Change < -1 * M1CHANGESWING) {
            Change = -1 * M1CHANGESWING;
        }

        // update duty cycle based on porportional control
        UR = UR + Change;
        UL = UL - Change;

        // so I think checking UR is sort of the speed check,
        // like how muc different in pwm can the two motors turn
        // check to ensure not too big of a swing
        if (UR < PWMIN) {
            UR = PWMIN;
        }
        if (UR > PWMAX) {
            UR = PWMAX;
        }
        if (UL < PWMIN) {
            UL = PWMIN;
        }
        if (UL > PWMAX) {
            UL = PWMAX;
        }
        //UL = 5000;
        //UR = 5000;

        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }
}

// proportional control, line following
void Program17_4(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();


    // initializes systik for the timer to see count the bumps
    SysTick->CTRL = 0x00000005; // enable SysTick with no interrupts
    SysTick->LOAD = 0x00ffffff; // loads max value
    SysTick->VAL = 0;          // any write to CVR clears it and COUNTFLAG in CSR

    //TimerA0_Init(&Backwards, 500);

    // user TimerA1 to run the controller at 1000 Hz, this is every 0.0001s
    TimerA1_Init(&Controller4, 500); // 500000 / [num in func call] = [hertz you want to operate at]

    Motor_Stop();
    LCDClear3();
    Mode = 0; // 0 is stop, 1 is forward, 2 is backwards
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();
    Pause3();
    while(1){
      if(Bump_Read()){ // collision
        //Mode = 0;
        //Motor_Stop();
        time = newTime;
        newTime = SysTick->VAL;
        if (newTime - time > 4800000) {
            bumpCount++;
        }
        //Pause3();
      }
      if (bumpCount == 1 && hasDone180 == 0) {
          hasDone180 = 1; // make sure you don't do this loop again
          Mode = 0; //turn off the line follower
          // back up if you've hit the bump
          Motor_Stop();
          Motor_Backward(6000, 6000);
          Clock_Delay1ms(500);
          Motor_Stop();

          //turn Left
          Motor_Right(8000, 8000);
          Clock_Delay1ms(700);
          Motor_Stop();
          Mode = 1;
      }
      if (bumpCount == 2) {
          Motor_Stop();
          Mode = 0;
          Motor_Stop();
          Cop_Car_Mode();
      }
      if(ControllerFlag){ // 100 Hz , not real time
        LCDOut3();
        ControllerFlag = 0;
      }
    }
}


// Maze Project Part 2
/**************Program17_5******************************************/

//uint8_t LineData;       // direct measure from line sensor
//int32_t Position;      // position in 0.1mm relative to center of line
#define M2CHANGEVAL 4    // the increment of change if the robot is too far left or right // worked with 4
#define M2CHANGESWING 1000 //worked with 3000
//time = 0; // compiler was yelling. should be 0 though
//newTime = 0;
//bumpCount = 0;
int tempDone = 0;
int IsDone = 0;
int LeftStepsComp = 0;
int RightStepsComp = 0;
int LastLeft = 0;
int LastRight = 0;
uint8_t firstRight = 1;
int backCounter = 0;
uint8_t lastLineData = 0;
int turn90Val = 90*150/70 - 25;
int turnSpeed = 3000;

//int32_t Change=0; // already defined
//volatile uint32_t systickCount = 0;   // already defined in 3
// 3700 and 1000 for the swing
void Controller5(void){

    // read values from line sensor, similar to
    // SysTick_Handler() in Lab10_Debugmain.c
    // Reftectance start on one systick ISR, End() on the next
        if (systickCount == 8) {
            Reflectance_Start();
        }
        else if (systickCount == 9) {
             // bits go left to right, 1 for black 0 for while so 0xc0 means the robot is to the right of the line with the left 2 sensors on black
            LineData = Reflectance_End() ^ 0xFF;
              //Bump = Bump_Read(); // not used in lab 17.3
        }
        if (systickCount == 9) {
            systickCount = 0;
        }
        else {
            systickCount++;
        }

        // Use Reflectance_Position() to find position
        Position =  Reflectance_Position(LineData);

        // Line Following
    if(Mode == 1){
        // switching to a linear controller because it is more stable and will
		//	yeild better line reading results to determine where the turns are
		//	this is in an effort to prevent the "drunk driving" robot from swining 
		//	back and forth
        if (Position > 0) {
            Change += 12;
        }

        // if robot is too far left of line (Position < 0)
        // decrease change
        else {
            Change -= 12;
        }

        // limit change to within swing
        if (Change > M2CHANGESWING) {
            Change = M2CHANGESWING;
        } else if (Change < -1 * M2CHANGESWING) {
            Change = -1 * M2CHANGESWING;
        }

        // update duty cycle based on porportional control
        UR = UR + Change;
        UL = UL - Change;

        // so I think checking UR is sort of the speed check,
        // like how muc different in pwm can the two motors turn
        // check to ensure not too big of a swing
        if (UR < PWMIN) {
            UR = PWMIN;
        }
        if (UR > PWMAX) {
            UR = PWMAX;
        }
        if (UL < PWMIN) {
            UL = PWMIN;
        }
        if (UL > PWMAX) {
            UL = PWMAX;
        }

        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }


    // Turn left
	// defunt, wasted too much time trying to do this. easier to just turn in the main
//    if (Mode == 2) {
//        Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
//
//        if (firstRight == 1) {
//            firstRight = 0;
//            UL = 3000;
//            UR = 3000;
//            LastLeft = LeftSteps;
//            LastRight = RightSteps;
//            Motor_Right(UL, UR);
//        }
//
//        //assumes (hopes) that steps are positive or negative
//        LeftStepsComp = (LeftSteps - LastLeft); // updates the total left steps
//        //LastLeft = LeftSteps;
//        RightStepsComp = (RightSteps - LastRight); // updates the total right steps
//        //LastRight = RightSteps;
//        if (RightStepsComp < turn90Val && LeftStepsComp > -1*turn90Val) { // right and left haven't turned enough
//            // turn right with same pwm
//            UL = turnSpeed;
//            UR = turnSpeed;
//        } else if (RightStepsComp > turn90Val && LeftStepsComp > -1*turn90Val) { // right has turned enough, left has not
//            UR = 0;
//            UL = turnSpeed;
//        } else if (RightStepsComp < turn90Val && LeftStepsComp < -1*turn90Val) { // right hasn't turned enough, left has;
//            // turn right with pwm of x, 0
//            UR = turnSpeed;
//            UL = 0;
//
//        } else if (RightStepsComp > turn90Val && LeftStepsComp < -1*turn90Val) { // both have turned enough
//            Motor_Stop();
//            IsDone = 1;
//            Mode = 1; // go back to line following
//            UL = 0;
//            UR = 0;
//            firstRight = 1;
//            tempDone = 1;
//        }
//        Motor_Right(UL, UR);
//    }
//
//    // Turn right
//        if (Mode == 3) {
//            Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
//
//            if (firstRight == 1) {
//                firstRight = 0;
//                UL = 3000;
//                UR = 3000;
//                LastLeft = LeftSteps;
//                LastRight = RightSteps;
//                Motor_Right(UL, UR);
//            }
//
//            //assumes (hopes) that steps are positive or negative
//            LeftStepsComp = (LeftSteps - LastLeft); // updates the total left steps
//            //LastLeft = LeftSteps;
//            RightStepsComp = (RightSteps - LastRight); // updates the total right steps
//            //LastRight = RightSteps;
//            if (RightStepsComp > -1*turn90Val && LeftStepsComp < turn90Val) { // right and left haven't turned enough
//                // turn right with same pwm
//                UL = turnSpeed;
//                UR = turnSpeed;
//            } else if (RightStepsComp < -1*turn90Val && LeftStepsComp < turn90Val) { // right has turned enough, left has not
//                UR = 0;
//                UL = turnSpeed;
//            } else if (RightStepsComp > -1*turn90Val && LeftStepsComp > turn90Val) { // right hasn't turned enough, left has;
//                // turn right with pwm of x, 0
//                UR = turnSpeed;
//                UL = 0;
//
//            } else if (RightStepsComp < -1*turn90Val && LeftStepsComp > turn90Val) { // both have turned enough
//                Motor_Stop();
//                IsDone = 1;
//                Mode = 1; // go back to line following
//                UL = 0;
//                UR = 0;
//                firstRight = 1;
//                tempDone = 1;
//            }
//            Motor_Left(UL, UR);
//        }
}

// proportional control, line following
void Program17_5(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();


    // initializes systik for the timer to see count the bumps
    SysTick->CTRL = 0x00000005; // enable SysTick with no interrupts
    SysTick->LOAD = 0x00ffffff; // loads max value
    SysTick->VAL = 0;          // any write to CVR clears it and COUNTFLAG in CSR

    //TimerA0_Init(&Backwards, 500);

    // user TimerA1 to run the controller at 1000 Hz, this is every 0.0001s
    TimerA1_Init(&Controller5, 500); // 500000 / [num in func call] = [hertz you want to operate at]

    Motor_Stop();
    LCDClear3();
    Mode = 0; // 0 is stop, 1 is forward, 2 is backwards
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();
    Pause3();
    Mode = 1; // start line following
    //uint8_t processLine = 0;
    while(1){
        // check for left turn
        //processLine = LineData & 0xe0;

        // Catches any time the robot goes off the line
        if (Mode == 1 && LineData == 0x00) {
            Mode = 0;
            Motor_Stop();
            //Pause3();
            Motor_Left(4000, 4000); // turns robot around so it can take a left
            Clock_Delay1ms(1500);    // when it gets back to the T-joint that it
            //Motor_Stop();         // turned left at
            Motor_Forward(5000, 5000);
            Clock_Delay1ms(220);    // to get it back on the line
            Motor_Stop();
            Mode = 1;
        }

		 //this if catches any left turn
        if (Mode == 1 && (LineData == 0xf8 || LineData == 0xfd || LineData == 0xf0)) {
            Mode = 0;
            Motor_Stop();
            Motor_Right(8000, 8000);
            Clock_Delay1ms(150);
            Motor_Forward(5000,5000);
            Clock_Delay1ms(115);
            Motor_Right(8000, 8000);
            Clock_Delay1ms(150);
            Motor_Forward(5000,5000);
            Clock_Delay1ms(200);
            Motor_Stop();
            Mode = 1;
        }



		// checks to see if the finish line has been reached
		if (Mode == 1 && (LineData == 0xdb || LineData == 0x6d || LineData == 0xd6)) {
			Mode = 0;
			Motor_Stop();
			Cop_Car_Mode();
		}  
    }
}


/*
 * final project part 3
 */
//**************************************************
// Proportional controller to drive straight with no
// sensor input
void Controller6(void){

    if(Mode){
        // pull tachometer information
        Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
        i++;
        if (i == TACHBUFF) {
            i = 0;
        }

        // use average of ten tachometer values to determine
        // actual speed (similar to Lab16)
        ActualSpeedL = 2000000 / avg(LeftTach, TACHBUFF);
        ActualSpeedR = 2000000 / avg(RightTach, TACHBUFF);


        // use proportional control to update duty cycle
        // LeftDuty = LeftDuty + Kp*LeftError
        ErrorR = DESIRED_SPEED - ActualSpeedR; // x* - x'
        ErrorL = DESIRED_SPEED - ActualSpeedL; // x* - x'

        /*
         *  so the problem araises when we calculate the Error for the ActualSpeedR, it is returned in RPM,
         *  but the UR is in terms of PWM, so
         */


        UR = UR + Kp1 * ErrorR;
        UL = UL + Kp1 * ErrorL;

        // check min/max duty values
        if (UR < 2) {
            UR = 2;
        }
        if (UR > 14998) {
            UR = 14998;
        }
        if (UL < 2) {
            UL = 2;
        }
        if (UL > 14998) {
            UL = 14998;
        }

        // update motor values
        Motor_Forward(UL, UR);
        ControllerFlag = 1;

    }
}

// go straight with no sensor input
void Program17_6(void){
  DisableInterrupts();
// initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Tachometer_Init();
  Motor_Init();

  // user TimerA1 to run the controller at 100 Hz
  // replace this line with a call to TimerA1_Init()
  TimerA1_Init(&Controller6, 5000); // 500000 / [num in func call] = [hertz you want to operate at]

  Motor_Stop();
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  LCDClear1();
  ControllerFlag = 0;
  Pause3();
  uint8_t bump = 0x00;

    while(1){
        bump = Bump_Read();
        if(bump){
            Mode = 0;
            Motor_Stop();
            //Pause3();

            // hits right
            if (Mode = 1 && Bump_Read() == 0x01) {
                Mode = 0;
                Motor_Stop();
                Motor_Backward(4000, 4000);
                Clock_Delay1ms(350); // back away from the wall
                Motor_Right(4000, 4000);
                Clock_Delay1ms(150);
                Motor_Stop();
                Mode = 1;
            }
            // hits left
            else if (Mode = 1 && Bump_Read() == 0x20) {
                Mode = 0;
                Motor_Stop();
                Motor_Backward(4000, 4000);
                Clock_Delay1ms(350); // back away from the wall
                Motor_Left(4000, 4000);
                Clock_Delay1ms(150);
                Motor_Stop();
                Mode = 1;
            }
            // hits straignt
            else if (Mode = 1 && Bump_Read()) {
                Mode = 0;
                Motor_Stop();
                Motor_Backward(4000, 4000);
                Clock_Delay1ms(450); // back away from the wall
                Motor_Right(8000, 8000);
                Clock_Delay1ms(300);
                Motor_Stop();
                Mode = 1;
            }
		}
		if(ControllerFlag){
			LCDOut1();
			ControllerFlag = 0;
		}
	}
}


/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
void SysTick_Handler(void){
    if(Mode){
//        // Determine set point
//        if (Left > 172 && Right > 172) {
//            SetPoint = (Left + Right) / 2;
//        }
//        else {
//            SetPoint = 172;
//        }
//
//        // set error based off set point
//        if (Left > Right) {
//            Error = Left - SetPoint;
//        }
//        else if (Right > Left) {
//            Error = SetPoint - Right;
//        }

        // use average of ten tachometer values to determine
        // actual speed (similar to Lab16)
//        ActualSpeedL = 2000000 / avg(LeftTach, TACHBUFF);
//        ActualSpeedR = 2000000 / avg(RightTach, TACHBUFF);

        // hug the right wall
        if (Right < 175) {
            Error = 175 - Right;
        }
        else if (Right > 175) {
            Error = 175 - Right;
        }

        // update duty cycle based on proportional control
        UR = UR + Kp2 * Error / 1;
        UL = UL - Kp2 * Error / 1;

        // check to ensure not too big of a swing
        if (UR < PWMIN) {
            UR = PWMIN;
        }
        if (UR > PWMAX) {
            UR = PWMAX;
        }
        if (UL < PWMIN) {
            UL = PWMIN;
        }
        if (UL > PWMAX) {
            UL = PWMAX;
        }

        // update motor values
        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }
}

// proportional control, wall distance
void Program17_7(void){
    uint32_t raw17,raw12,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Motor_Init();

    // user TimerA1 to sample the IR sensors at 2000 Hz
    TimerA1_Init(&IRsampling, 250); // 500000 / [num in func call] = [hertz you want to operate at]

    Motor_Stop();
    LCDClear2();
    Mode = 0;
    UR = UL = PWMNOMINAL;
    ADCflag = ControllerFlag = 0;   // semaphores

    ADC0_InitSWTriggerCh17_12_16();   // initialize channels 17,12,16
    ADC_In17_12_16(&raw17,&raw12,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw12,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

    // user SysTick to run the controller at 100 Hz with a priority of 2
    // replace this line with a call to SysTick_Init()
    SysTick_Init(480000, 2);
    //SysTick_Init();


    Pause3();

    EnableInterrupts();
    Mode = 1;
    while(1){
        if(Bump_Read()){ // collision
            Mode = 0;
            Motor_Stop();
            Pause3();
        }

        //left only strategy

        // hits a left turn, take the left turn
        if (Center < 130) {
            Mode = 0;
            Motor_Stop();
            Motor_Right(4000, 4000);
            Clock_Delay1ms(150);
            Motor_Stop();
            Mode = 1;
        }
        if(ControllerFlag){ // 100 Hz, not real time
            LCDOut2();
            ControllerFlag = 0;
        }
    }
}




int main(void){
//    Program17_1();
//    Program17_2();
//    Program17_3();
//      Program17_4();
//    Program17_5();
//    Program17_6();
    Program17_7();


}
