; LEDs.asm
; Runs on MSP432
; Capt Steven Beyer
; September 9, 2019


;	Code to activate LED on P5.4. This code accompanies the Lab08_LED_Switchesmain.c
;
       .thumb
       .text
       .align 2
       .global LED_Init
       .global LED_Off
       .global LED_On
       .global LED_Toggle
       .global LED_Oscillate
       .global Loop_Start	;idk if local is a thing but this should be local
       .global Loop_Init	;same here
       .global Turn_On
       .global Turn_Off
       .global Done



; function to initialize P5.4
LED_Init:	.asmfunc
	LDR R1, P5SEL0
	LDRB R0, [R1]
	BIC R0, R0, #0x10	; GPIO
	STRB R0, [R1]
	LDR R1, P5SEL1
	LDRB R0, [R1]
	BIC R0, R0, #0x10
	STRB R0, [R1]		; GPIO
	LDR R1, P5DIR
	LDRB R0, [R1]
	ORR R0, R0, #0x10	; output
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to turn off P5.4
LED_Off:		.asmfunc
	LDR R1, P5OUT
	LDRB R0, [R1]		; 8-bit read
	BIC R0, R0, #0x10	; turn off
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to turn on P5.4
LED_On:	.asmfunc
	LDR R1, P5OUT
	LDRB R0, [R1]		; 8-bit read
	ORR R0, R0, #0x10	; turn on
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to toggle P5.4
LED_Toggle: .asmfunc
	PUSH {LR}

	LDR R1, P5OUT		; load address of P5OUT into R1
	LDRB R0, [R1]		; 8-bit read, Reads P5 into R0--i think...
	AND R0, R0, #0x10	;
	CMP R0, #0x00		; if the light is on, R0 should be positive, so this compair will not set the 0 flag
	; so b is a jump that does not set LR meaning there's no going back
	; BL stores the next instruction in this frame to LR so you can return, meaning im going to have to store the current value of LR safely on the stack
	BEQ Turn_On
	B Turn_Off
Turn_On:
	BLEQ LED_On
	B Done

Turn_Off:
	BLNE LED_Off

Done:
	POP {LR}
	BX LR




	.endasmfunc

; function to continuously toggle P5.4 every half second
; use a loop as a timer
LED_Oscillate:	.asmfunc
	PUSH {LR}			; gonna be calling functions (which overwrite the LR) so save it on the stack
	BL LED_On			; start by turning it on,
	MOV R1, #0x00000011

Loop_Init:
	LDR R0, DELAY		; load loop counter of 12 000 000 to R0

Loop_Start:
	SUB R0, R0, #0x1	; dec the loop counter
	CMP R0, #0x0
	BNE Loop_Start		; jump to loop start (past initialization) if the loop counter isnt 0
	BL LED_Toggle
	B Loop_Init			; infinite loop

	POP {LR}			; restore LR so we can leave the function
	.endasmfunc
; addresses for Port 5 registers
	.align 4
P5SEL0 .field 0x40004C4A,32
P5SEL1 .field 0x40004C4C,32
P5DIR  .field 0x40004C44,32
P5OUT  .field 0x40004C42,32
;DELAY  .field 0xB71B00,32
DELAY  .field 2666035,32
;used logic analyser to find this value. 499.9 ms (0.4999s)

	.end
