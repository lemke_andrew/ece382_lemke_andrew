// Lab16_Tachmain.c
// Runs on MSP432
// Test the operation of the tachometer by implementing
// a simple DC motor speed controller.
// Daniel Valvano
// September 5, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// Pololu #3542 Romi Encoder connected to Pololu #3543 Motor Driver and Power Distribution Board
//   This connects motor, power, encoders, and grounds.  Kit includes this hardware.  See images.
// Sever VPU = VREG jumper on Motor Driver and Power Distribution Board and connect VPU to 3.3V.
//   This is necessary because MSP432 inputs are not 5V tolerant.
// Left Encoder A connected to P8.2 (J5)
// Left Encoder B connected to P9.2 (J5)
// Right Encoder A connected to P10.4 (J5)
// Right Encoder B connected to P10.5 (J5)

// Sever VCCMD=VREG jumper on Motor Driver and Power Distribution Board and connect VCCMD to 3.3V.
//   This makes P3.7 and P3.6 low power disables for motor drivers.  0 to sleep/stop.
// Sever nSLPL=nSLPR jumper.
//   This separates P3.7 and P3.6 allowing for independent control
// Left motor direction connected to P1.7 (J2.14)
// Left motor PWM connected to P2.7/TA0CCP4 (J4.40)
// Left motor enable connected to P3.7 (J4.31)
// Right motor direction connected to P1.6 (J2.15)
// Right motor PWM connected to P2.6/TA0CCP3 (J4.39)
// Right motor enable connected to P3.6 (J2.11)

// See Bump.c for bumper connections (Port 8 or Port 4)

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/LaunchPad.h"
#include "../inc/Motor.h"
#include "../inc/Nokia5110.h"
#include "../inc/Tachometer.h"
#include "../inc/TimerA1.h"
#include "../inc/TA3InputCapture.h"
#include "../inc/TExaS.h"
#include "../inc/FlashProgram.h"
#include "../inc/Bump.h"

#define P2_4 (*((volatile uint8_t *)(0x42098070)))
#define P2_3 (*((volatile uint8_t *)(0x4209806C)))
#define P2_2 (*((volatile uint8_t *)(0x42098068)))
#define P2_1 (*((volatile uint8_t *)(0x42098064)))
#define P2_0 (*((volatile uint8_t *)(0x42098060)))

uint16_t Period0;              // (1/SMCLK) units = 83.3 ns units
uint16_t First0;               // Timer A3 first edge, P10.4
int Done0;                     // set each rising
// max period is (2^16-1)*83.3 ns = 5.4612 ms
// min period determined by time to run ISR, which is about 1 us
void PeriodMeasure0(uint16_t time){
  P2_0 = P2_0^0x01;           // thread profile, P2.0
  Period0 = (time - First0)&0xFFFF; // 16 bits, 83.3 ns resolution
  First0 = time;                   // setup for next
  Done0 = 1;
}
uint16_t Period2;              // (1/SMCLK) units = 83.3 ns units
uint16_t First2;               // Timer A3 first edge, P8.2
int Done2;                     // set each rising
// max period is (2^16-1)*83.3 ns = 5.4612 ms
// min period determined by time to run ISR, which is about 1 us
void PeriodMeasure2(uint16_t time){
  P2_4 = P2_4^0x01;           // thread profile, P2.4
  Period2 = (time - First2)&0xFFFF; // 16 bits, 83.3 ns resolution
  First2 = time;                   // setup for next
  Done2 = 1;
}
int Program16_1(void){
  Clock_Init48MHz();   // 48 MHz clock; 12 MHz Timer A clock
  P2->SEL0 &= ~0x11;
  P2->SEL1 &= ~0x11;   // configure P2.0 and P2.4 as GPIO
  P2->DIR |= 0x11;     // P2.0 and P2.4 outputs
  First0 = First2 = 0; // first will be wrong
  Done0 = Done2 = 0;   // set on subsequent
  Motor_Init();        // activate Lab 13 software
  TimerA3Capture_Init(&PeriodMeasure0,&PeriodMeasure2);
  Motor_Forward(7500,7500); // 50%
  EnableInterrupts();
  while(1){
    WaitForInterrupt();
  }
}
uint16_t SpeedBuffer[500];      // RPM
uint32_t PeriodBuffer[500];     // 1/12MHz = 0.083 usec
uint32_t Duty,DutyBuffer[500];  // 0 to 15000
uint32_t Time; // in 0.01 sec
void Collect(void){
  P2_1 = P2_1^0x01;    // thread profile, P2.1
  if(Done0==0) Period0 = 65534; // stopped
  if(Done2==0) Period2 = 65534; // stopped
  Done0 = Done2 = 0;   // set on subsequent
  if(Time==100){       // 1 sec
    Duty = 7500;
    Motor_Forward(7500,7500);    // 50%
  }
  if(Time==200){       // 2 sec
    Duty = 11250;
    Motor_Forward(11250,11250);  // 75%
  }
  if(Time==300){       // 3 sec
    Duty = 7500;
    Motor_Forward(7500,7500);    // 50%
  }
  if(Time==400){       // 4 sec
    Duty = 3750;
    Motor_Forward(3750,3750);    // 25%
  }
  if(Time<500){       // 5 sec
    SpeedBuffer[Time] = 2000000/Period0;
    PeriodBuffer[Time] = Period0;
    DutyBuffer[Time] = Duty;
    Time = Time+1;
  }
  if((Time==500)||Bump_Read()){
    Duty = 0;
    Motor_Stop();     // 0%
    TimerA1_Stop();
  }
}
int Program16_2(void){
  DisableInterrupts();
  Clock_Init48MHz();   // 48 MHz clock; 12 MHz Timer A clock
  P2->SEL0 &= ~0x11;
  P2->SEL1 &= ~0x11;   // configure P2.0 and P2.4 as GPIO
  P2->DIR |= 0x11;     // P2.0 and P2.4 outputs
  First0 = First2 = 0; // first will be wrong
  Done0 = Done2 = 0;   // set on subsequent
  Time = 0; Duty = 3750;
  Bump_Init();
  Motor_Init();        // activate Lab 13 software
  TimerA3Capture_Init(&PeriodMeasure0,&PeriodMeasure2);
  TimerA1_Init(&Collect,5000); // 100 Hz
  Motor_Forward(3750,3750); // 25%
  TExaS_Init(LOGICANALYZER_P10);
  EnableInterrupts();
  while(1){
    WaitForInterrupt();
  }
}
#define FLASH_BANK1_MIN     0x00020000  // Flash Bank1 minimum address
#define FLASH_BANK1_MAX     0x0003FFFF  // Flash Bank1 maximum address
void Debug_FlashInit(void){ uint32_t addr;
  Flash_Init(48);
  for(addr=FLASH_BANK1_MIN;addr<0x0003FFFF;addr=addr+4096){
    if(Flash_Erase(addr)==ERROR){
      while(1){
        LaunchPad_Output(BLUE);  Clock_Delay1ms(200);
        LaunchPad_Output(RED);  Clock_Delay1ms(500);
        LaunchPad_Output(GREEN);  Clock_Delay1ms(300);
      }
    }
  }
}
// record 32 halfwords
void Debug_FlashRecord(uint16_t *pt){uint32_t addr;
  addr=FLASH_BANK1_MIN;
  while(*(uint32_t*)addr != 0xFFFFFFFF){ // find first free block
    addr=addr+64;
    if(addr>FLASH_BANK1_MAX) return; // full
  }
  Flash_FastWrite((uint32_t *)pt, addr, 16); // 16 words is 32 halfwords, 64 bytes
}
int Program16_3(void){//main2(void){
int i;
  DisableInterrupts();
  Clock_Init48MHz();   // 48 MHz clock; 12 MHz Timer A clock
  LaunchPad_Init();
  if(LaunchPad_Input()){
    LaunchPad_Output(RED);
    Debug_FlashInit(); // erase flash if either switch pressed
    while(LaunchPad_Input()){}; // wait for release
  }
  P2->SEL0 &= ~0x11;
  P2->SEL1 &= ~0x11;   // configure P2.0 and P2.4 as GPIO
  P2->DIR |= 0x11;     // P2.0 and P2.4 outputs
  First0 = First2 = 0; // first will be wrong
  Done0 = Done2 = 0;   // set on subsequent
  Time = 0; Duty = 3750;
  Bump_Init();
  Motor_Init();        // activate Lab 13 software
  TimerA3Capture_Init(&PeriodMeasure0,&PeriodMeasure2);
  TimerA1_Init(&Collect,5000); // 100 Hz
  Motor_Forward(3750,3750); // 25%
  TExaS_Init(LOGICANALYZER_P10);
  EnableInterrupts();
  while(1){
    WaitForInterrupt();
    if(Time>=500){
      LaunchPad_Output(GREEN);
      for(i=0;i<16;i++){
        Debug_FlashRecord(&SpeedBuffer[32*i]);
      }
      while(1){
        LaunchPad_Output(BLUE);  Clock_Delay1ms(200);
     }
    }
  }
}

#define DESIREDMAX 120
#define DESIREDMIN 30
#define PWMMAX 14998
#define PWMMIN 0
// variables for controlling motors
int16_t DesiredL = 50;					// desired RPM left, set initially to 50
int16_t DesiredR = 50;					// desired RPM right, set initially to 50
uint16_t ActualL;                       // actual rotations per minute
uint16_t ActualR;                       // actual rotations per minute
uint16_t LeftDuty = 3750;               // duty cycle of left wheel (0 to 14,998)
uint16_t RightDuty = 3750;              // duty cycle of right wheel (0 to 14,998)

int avgLeft = 0;
int avgRight = 0;

// variables to average tachometer readings 
#define TACHBUFF 10
uint16_t LeftTach[TACHBUFF];			// tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;
int32_t LeftSteps;
uint16_t RightTach[TACHBUFF]; //changed to 32 from 16
enum TachDirection RightDir;
int32_t RightSteps;

void LCDClear(void){
	Nokia5110_Init();
	Nokia5110_Clear();
	Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Distance(mm)");
}

void LCDDesired(void){
	Nokia5110_SetCursor(1, 1);         // one leading space, second row
	Nokia5110_OutSDec1(DesiredL);
	Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
	Nokia5110_OutSDec1(DesiredR);
}

void LCDActual(void){
	Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
	Nokia5110_OutUDec(ActualL);
    //Nokia5110_OutUDec(66);

	Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
	Nokia5110_OutUDec(ActualR);
	Nokia5110_SetCursor(0, 5);       // zero leading spaces, sixth row
	if(LeftSteps < 0){
		Nokia5110_OutChar('-');
		// 70mm diameter wheel; ~220mm circumference divided by 360 steps
		// replace this line with code to print negative left distance to LCD
		Nokia5110_OutUDec((-1*LeftSteps*220)/360);
	}else{
		Nokia5110_OutChar(' ');
		// 70mm diameter wheel; ~220mm circumference divided by 360 steps
		// replace this line with code to print positive left distance to LCD
        Nokia5110_OutUDec((LeftSteps*220)/360); // assumed it is the same as above but you don't multiply by negative 1
	}
	Nokia5110_SetCursor(6, 5);       // six leading spaces, sixth row
	if(RightSteps < 0){
		Nokia5110_OutChar('-');
		// 70mm diameter wheel; ~220mm circumference divided by 360 steps
		// replace this line with code to print negative right distance to LCD
		Nokia5110_OutUDec((-1*RightSteps*220)/360);
	}else{
		Nokia5110_OutChar(' ');
		// 70mm diameter wheel; ~220mm circumference divided by 360 steps
		// replace this line with code to print positive right distance to LCD
        Nokia5110_OutUDec((RightSteps*220)/360); // assumed it is the same as above but you don't multiply by negative 1

	}
}

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){ // changed to 32 from 16 bit
    int i;
    uint32_t sum = 0;
    // write code to return the average
    for (i = 0; i < length; i++) {
        sum += array[i];
    }
    //int avg = sum / length;
    return (sum / length);
}
uint16_t abs(int16_t val) {
    if (val < 0) {
        return -val;
    }
    else {
        return val;
    }
}



void Program16_4(void){
	// write a main program that uses the tachometer
	int i = 0;
	Clock_Init48MHz();
	Tachometer_Init();
	LCDClear();
	LaunchPad_Init();
	Bump_Init();
	Tachometer_Init();
	Motor_Init();
	EnableInterrupts();
	//int32_t left_steps = 0;
	//int32_t right_steps = 0;

	//maybe initialize the timer with the motor controls like for lab 14 or just use the motor_forward
	// from the motor.c file. looks promising


	while(1){
		Motor_Stop();
		while(Bump_Read() == 0){
			// update the screen
			LCDDesired();
			
			if((LaunchPad_Input()&0x01) != 0x00){
			// Button1 has been pressed - increase desired right speed by 10 and rollover if max is reached
			    DesiredR += 10;
			    if (DesiredR > 120) {
			        DesiredR = -120;
			    }
			    if (DesiredR == -30) {
			        DesiredR = 40;
			    }
			    if (DesiredR == 30) {
			        DesiredR = -40;
			    }
			    if (DesiredR < -120){
			        DesiredR = 120;
			    }
			}
			
			if((LaunchPad_Input()&0x02) != 0x00){
			// Button2 has been pressed - increase desired left speed by 10 and rollover if max is reached
			    DesiredL += 10;
                if (DesiredL > 120) {
                    DesiredL = -120;
                }
                if (DesiredL == -30) {
                    DesiredL = 40;
                }
                if (DesiredL == 30) {
                    DesiredL = -40;
                }
                if (DesiredL < -120){
                    DesiredL = 120;
                }
			}
			
			// flash the blue LED
			i = i + 1;
			LaunchPad_Output((i&0x01)<<2);
			Clock_Delay1ms(200);               // delay ~0.2 sec at 48 MHz
		}

		for(i=0; i<10; i=i+1){
			// flash the yellow LED
			LaunchPad_Output(0x03);
			Clock_Delay1ms(100);               // delay ~0.1 sec at 48 MHz
			LaunchPad_Output(0x00);
			Clock_Delay1ms(100);               // delay ~0.1 sec at 48 MHz
		}

		LaunchPad_Output(0x02);
		i = 0;

		while(Bump_Read() == 0) {
			// get values from the tachometer and store into variables
			// replace this line with function call to Tachometer_Get()
			//uint8_t tempRead = Bump_Read();

		        //problem may be passing wrong value in or something
            Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
            //use LeftTach[i] or &LeftTach[i]

			i = i + 1;
			if(i >= TACHBUFF){
				i = 0;
				// (1/tach step/cycles) * (12,000,000 cycles/sec) * (60 sec/min) * (1/360 rotation/step)
				// write code to get actual left and right values
				//int avgL = avg(LeftTach, TACHBUFF);
				//int avgR = avg(RightTach, TACHBUFF);
				ActualL = 2000000 / avg(LeftTach, TACHBUFF);
				ActualR = 2000000 / avg(RightTach, TACHBUFF);
				
				// very simple, very stupid controller
				// if actual is greater than (desired + 3) and leftduty is 100 more than min
				// subtract 100 from left duty
				if ((ActualL > (abs(DesiredL) + 3)) && (LeftDuty > 100 + PWMMIN)) {
				    LeftDuty -= 100;
				}
				
				// if actual is less than (desired - 3) and leftduty is 100 less than max
				// add 100 to left duty
				if ((ActualL < (abs(DesiredL) - 3)) && (LeftDuty < PWMMAX - 100)) {
				    LeftDuty += 100;
				}
				
				// repeat for right
                // if actual is greater than (desired + 3) and rightduty is 100 more than min
                // subtract 100 from right duty
                if ((ActualR > (abs(DesiredR) + 3)) && (RightDuty > 100 + PWMMIN)) {
                    RightDuty -= 100;
                }

                // if actual is less than (desired - 3) and rightduty is 100 less than max
                // add 100 to right duty
                if ((ActualR < (abs(DesiredR) - 3)) && (RightDuty < PWMMAX - 100)) {
                    RightDuty += 100;
                }

				
				// drive motors forward
                if (DesiredL > 0 && DesiredR > 0) {
                    Motor_Forward(LeftDuty, RightDuty);
                }
                if (DesiredL < 0 && DesiredR > 0) {
                    Motor_Right(LeftDuty, RightDuty);
                }
                if (DesiredL > 0 && DesiredR < 0) {
                    Motor_Left(LeftDuty, RightDuty);
                }
                if (DesiredL < 0 && DesiredR < 0) {
                    Motor_Backward(LeftDuty, RightDuty);
                }

				// update the screen
				LCDActual();
			}
			
			Clock_Delay1ms(100);			// delay ~0.1 sec at 48 MHz
		}
		Motor_Stop();
		i = 0;
		while(Bump_Read() != 0x00){ // originally bump !=0x3f
			i = i + 1;
			LaunchPad_Output(i&0x01);
			Clock_Delay1ms(100);			// delay ~0.1 sec at 48 MHz
		}
	}
}

void main(void){
//    Program16_1();
//    Program16_2();
//    Program16_3();
    Program16_4();
}
