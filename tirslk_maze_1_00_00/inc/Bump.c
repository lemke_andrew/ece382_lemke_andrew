// Bump.c
// Runs on MSP432
// Provide low-level functions that interface bump switches the robot.
// Additional comments by Capt Steven Beyer
// 7 Aug 2019
// Daniel Valvano and Jonathan Valvano
// July 2, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// Negative logic bump sensors
// P4.7 Bump5, left side of robot
// P4.6 Bump4
// P4.5 Bump3
// P4.3 Bump2
// P4.2 Bump1
// P4.0 Bump0, right side of robot

#include <stdint.h>
#include "msp.h"


/******************************************************************************
              PIN INFO
    Robot facing away from you
Left Side
Left     blue    4.7
mid      green   4.6
right    yellow  4.5
Right Side
left     orange  4.3
mid      red     4.2
right    brown   4.0


    bit mask
4.0 to 4.7:  1011 0111
in hex: 0xb7

 */


// Initialize Bump sensors
// Make six Port 4 pins inputs
// Activate interface pullup
// SEE BELOW FOR WHICH PINS GO WHERE
void Bump_Init(void){
    // write this as part of Lab 10.4.2
    // bumps on 4.0-7
    // 1011 1110
    P4->SEL0 &= ~0xed; // make them all input
    P4->SEL1 &= ~0xed;
    P4->DIR &= ~0xed;
    P4->REN |= 0xed; //make them pull resistors
    P4->OUT |= 0xed; // make them pull up
}


// Read current state of 6 switches
// Returns a 6-bit positive logic result (0 to 63) 1 if not pressed, 0 if pressed, 0 for unused bits
// bit 5 Bump5
// bit 4 Bump4
// bit 3 Bump3
// bit 2 Bump2
// bit 1 Bump1
// bit 0 Bump0
uint8_t Bump_Read(void){
    // write this as part of Lab 10.4.2

	uint8_t orig_bump = P4->IN & 0xed;
	uint8_t ret_bump = 0x00;

	// put pin 4.0 in location 0;
	ret_bump |= (orig_bump & 0x01);

	// put pins 4.2, 4.3 in locations 1, 2
	// mask for 4.2 and 4.3: 0000 1100. in hex: 0x0c
	ret_bump |= ((orig_bump & 0x0c) >> 1);

	// put pins 4.5, 4.6, 4.7 in locations 3, 4, 5
	// mask: 1110 0000 = 0xe0
	//uint8_t temp = (orig_bump & 0xe0);
    ret_bump |= ((orig_bump & 0xe0) >> 2);

    // make it positive logic
    ret_bump = ~ret_bump & ~0xc0;
    return ret_bump;
}

